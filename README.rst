=====================================
The FEniCS Project Python Metapackage
=====================================

This package contains a single file, ``setup.py``, that allows all of the
FEniCS Python components to be installed from PyPI using ``pip``::

    pip3 install fenics

